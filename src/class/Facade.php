<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Session;

use Nora\Core\Module;
use Nora\Core\Util\SpecLine;

/**
 * Sessionモジュール
 */
class Facade extends Session implements Module\ModuleIF
{
    use Module\Modulable;

    const DEFAULT_HANDLER='file:///tmp/nora-session;length=32&name=NORASESSION';
    const DEFAULT_COOKIE_EXPIRE=0;
    const DEFAULT_COOKIE_PATH='/';
    const DEFAULT_COOKIE_DOMAIN='/';

    protected function initModuleImpl( )
    {
        parent::initComponentImpl();

        // 初期設定
        $handler         = $this->readConf('session.handler', self::DEFAULT_HANDLER);
        $cookie_expire   = $this->readConf('session.cookie_expire', self::DEFAULT_COOKIE_EXPIRE);
        $cookie_path     = $this->readConf('session.cookie_path', self::DEFAULT_COOKIE_PATH);
        $cookie_domain   = $this->readConf('session.cookie_domain', $this->getEnv('HTTP_HOST', 'localhost'));
        $cookie_secure   = $this->readConf('session.cookie_secure', false);
        $cookie_httponly = $this->readConf('session.cookie_httponly', true);

        // 設定
        $setting = compact(
            'handler',
            'cookie_expire',
            'cookie_path',
            'cookie_domain',
            'cookie_secure',
            'cookie_httponly'
        );

        $this->setOption($setting);
    }
}
