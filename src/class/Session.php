<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Session;

use Nora\Core\Module\Module;
use Nora\Core\Component\Component;
use Nora\Core\Util\Collection\Hash;
use Nora\Core\Options\OptionsAccess;

/**
 * Sessionコンポーネント
 */
class Session extends Component
{
    use OptionsAccess;

    private $_session_id = null;
    private $_data;

    protected function initComponentImpl( )
    {
        $this->rootScope()->setComponent('@Session', $this);

        $this->initOptions([
            'handler' => 'file:///tmp/nora-session;length=16',
            'name' => 'SESSIONID',
            'length' => 32,
            'cookie_expire' => 0,
            'cookie_path' => '/',
            'cookie_domain' => $this->getEnv('HTTP_HOST', 'localhost'),
            'cookie_secure' => false,
            'cookie_httponly' => true
        ]);
    }

    public function bootHandler( )
    {
        $spec = $this->getOption('handler');
        $kvs = $this->KVS_Create($spec, true);
        return $kvs;
    }


    /**
     * セッションを開始する
     */
    public function start ($id = null)
    {
        if ($this->_session_id !== null) {
            return $this->_session_id;
        }

        if (!$this->handler()->open( ))
        {
            $this->alert('セッションが開けません');
        }

        if ($id === null || !$this->handler()->has($id))
        {
            if ($this->environment_cookie()->has($this->getOption('name')))
            {
                $id = $this->environment_cookie()->get($this->getOption('name'));
            }else{
                $id = $this->gennewid();
            }
        }
        $this->_session_id = $id;

        $this->logDebug(["SESSION-ID" => $id], 'session.start');

        if($this->handler( )->has($this->_session_id))
        {
            $data = $this->handler( )->read($this->_session_id);
        }else{
            $data = [];
        }
        $this->_data = empty($data) ? []: unserialize($data);

        // 終了時の処理を設定
        register_shutdown_function(function ( ) {
            $this->close();
        });

        return $this->_session_id;
    }

    /**
     * セッションをクッキーへ格納する
     */
    public function pushCookie ( )
    {
        // セッションをクッキーへ格納する
        $this->environment_php()->setcookie ( 
            $this->getOption('name'),
            $this->_session_id,
            $this->getOption('cookie_expire'),
            $this->getOption('cookie_path'),
            $this->getOption('cookie_domain'),
            $this->getOption('cookie_secure'),
            $this->getOption('cookie_httponly')
        );
    }

    /**
     * 現在のセッションIDを取得する
     */
    public function sessionId( )
    {
        return $this->_session_id;
    }

    /**
     * 新しいセッションIDを生成する
     */
    protected function gennewid( )
    {
        do 
        {
            $id = $this->genid();

        } while($this->handler()->has($id));

        return $id;
    }

    /**
     * セッションIDを生成する
     */
    protected function genid( )
    {
        return $this->secure_randomString($this->getOption('length'));
    }

    /**
     * セッションを破棄する
     */
    public function destroy ( )
    {
        if (!$this->handler()->delete($this->_session_id))
        {
            $this->alert('SESSION Can not destroyed: '.$this->_session_id);
            return false;
        }

        $this->logDebug('SESSION Destroy: '.$this->_session_id);
        $this->_session_id = null;
        $this->_data = [];
    }

    /**
     * 現在のセッションIDを廃棄して
     * 新しいセッションIDを取得する
     */
    public function regen( )
    {
        $newid = $this->gennewid();
        $data = $this->_data;
        $this->destroy();
        $this->_session_id = $newid;
        $this->_data = $data;

        // クッキーを再送する
        $this->pushCookie();

        return $this->_session_id;
    }

    /**
     * セッションを閉じる
     */
    public function close( )
    {
        if ($this->_session_id === null) return false;

        $result = $this->handler( )->write($this->_session_id, serialize($this->_data));

        $this->_session_id = null;
        $this->_data = [];

        return $result;
    }

    public function __isset($name)
    {
        return $this->has($name, $value);
    }

    public function __set($name, $value)
    {
        $this->set($name, $value);
    }

    public function &__get($name)
    {
        return $this->get($name);
    }

    public function has($name)
    {
        return isset($this->_data[$name]);
    }

    public function set($name, $value)
    {
        $this->_data[$name] = $value;
    }

    public function &get($name, $value = null)
    {
        if ($this->has($name))
        {
            return $this->_data[$name];
        }
        return $value;
    }
}
