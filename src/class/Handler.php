<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Session;

use Nora\Core\Module\Module;
use Nora\Core\Component\Component;

/**
 * Session Handler
 */
abstract class Handler implements \SessionHandlerInterface
{
    const CLASS_FORMAT = __namespace__.'\\Handler\\%s';

    static public function build ($type)
    {
        $class = sprintf(self::CLASS_FORMAT, ucfirst($type));
        return new $class();
    }


    protected function __construct( )
    {
        $this->initHandler();
    }

    protected function initHandler( )
    {
        $this->initHandlerImpl();
    }

    protected function initHandlerImpl( )
    {
    }
}

