<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Session\Handler;

use Nora\Core\Module\Module;
use Nora\Core\Component\Component;
use Nora\Module\Session\Handler;

/**
 * Session Handler
 */
class File extends Handler
{

    protected function initHandlerImpl ( )
    {
    }

    public function isExists($id)
    {
        $file = $this->getFile($id);

        return file_exists($file);
    }

    public function read($id)
    {
        if (!$this->isExists($id)) return null;
        return file_get_contents($this->getFile($id));
    }

    public function write($id, $data)
    {
        return file_put_contents($this->getFile($id), $data);
    }

    public function destroy($id)
    {
        if (!$this->isExists($id)) return false;
        return unlink($this->getFile($id));
    }

    private function getFile($id)
    {
        return sprintf("%s/%s-%s",
            $this->_path,
            $this->_name,
            $id
        );
    }

    public function open ($path, $name)
    {
        $this->_name = $name;
        $this->_path = $path;

        if (!is_dir($path))
        {
            if (!mkdir ($path, 0777))
            {
                $this->alert("$pathが作成できません");
                return false;
            }
        }
        return true;
    }

    public function gc($maxlifetime)
    {
        foreach (glob("$this->savePath/sess_*") as $file) {
            if (filemtime($file) + $maxlifetime < time() && file_exists($file)) {
                unlink($file);
            }
        }

        return true;
    }

    public function close ( )
    {
        return true;
    }

}


