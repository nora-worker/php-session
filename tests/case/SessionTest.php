<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Session;

use Nora;

/**
 * SessionModuleのテスト
 *
 */
class SessionTest extends \PHPUnit_Framework_TestCase
{
    public function testSecure ( )
    {
        Nora::environment_php()->header = function ($header) {
            Nora::logDebug(['header' => $header]);
        };
        Nora::environment_php()->setcookie = function ( ) {
            Nora::logDebug(func_get_args(), 'cookie');
        };

        Nora::session_start();

        // セッションの開始
        $id_1 = Nora::Session()->start();
        Nora::Session( )->name = 'hajime';
        Nora::Session( )->close();

        // セッションの継続
        $id_2 = Nora::Session()->start($id_1);
        $this->assertEquals($id_1, $id_2);
        $this->assertEquals('hajime', Nora::Session( )->name);

        // セッションIDの再生成
        $id_3 = Nora::Session()->regen();
        Nora::Session( )->close();

        // 以前のIDではアクセスできない
        Nora::Session()->start($id_1);
        $this->assertFalse('hajime' === Nora::Session( )->name);
        Nora::Session( )->close();

        $this->assertFalse($id_1 === $id_3);

        // 新しいIDでのセッションの継続
        Nora::Session()->start($id_3);
        $this->assertEquals('hajime', Nora::Session( )->name);
        Nora::Session( )->close();
    }
}
